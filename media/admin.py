from django.contrib import admin

# Register your models here.
from media.models import Media

admin.site.register(Media)
