
from django.contrib import admin
from django.urls import include, path

# urls
urlpatterns = [
    path('api/v1/startup-project/', include('startup_project.urls')),
    path('admin/', admin.site.urls),
]