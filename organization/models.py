from django.db import models

# Create your models here.


class Organization(models.Model):
    PERSONAL_LESS_THAN_20 = 'less_than_20'
    PERSONAL_MORE_20_LESS_100 = 'less_than_100_more_than_20'
    PERSONAL_MORE_100_LESS_500 = 'more_than_100_less_than_500'
    PERSONAL_MORE_THAN_500 = 'more_than_500'
    PERSONAL_COUNT = {
        (0, PERSONAL_LESS_THAN_20),
        (1, PERSONAL_MORE_20_LESS_100),
        (2, PERSONAL_MORE_100_LESS_500),
        (3, PERSONAL_MORE_THAN_500),
    }
    name = models.CharField(max_length=1500)
    address = models.CharField(max_length=5000)
    inn = models.CharField(max_length=12)
    personal_count = models.IntegerField(choices=PERSONAL_COUNT, default=None, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.name + " (" + self.inn + ")"