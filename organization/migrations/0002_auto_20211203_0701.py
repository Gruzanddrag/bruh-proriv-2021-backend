# Generated by Django 3.1.8 on 2021-12-03 07:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organization', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organization',
            name='personal_count',
            field=models.IntegerField(choices=[('less_than_20', 0), ('less_than_100_more_than_20', 1), ('more_than_500', 3), ('more_than_100_less_than_500', 2)], default=None, null=True),
        ),
    ]
