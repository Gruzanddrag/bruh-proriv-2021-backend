from django.shortcuts import render

# Create your views here.
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView

from startup_project.models import StartupProject
from startup_project.serializers import StartupProjectSerializer, StartupProjectFullSerializer
from rest_framework.pagination import PageNumberPagination
from django_filters import rest_framework as filters


class CustomPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'


# We create filters for each field we want to be able to filter on
class StartupProjectFilter(filters.FilterSet):
    title = filters.CharFilter(lookup_expr='icontains')
    direction = filters.NumberFilter()

    class Meta:
        model = StartupProject
        fields = ['title', 'direction']


class ListCreateStartupsAPIView(ListCreateAPIView):
    serializer_class = StartupProjectSerializer
    queryset = StartupProject.objects.all()
    permission_classes = []
    pagination_class = CustomPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filterset_class = StartupProjectFilter

    def perform_create(self, serializer):
        # Assign the user who created the movie
        serializer.save(creator=self.request.user)


class RetrieveUpdateDestroyStartupsAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = StartupProjectFullSerializer
    queryset = StartupProject.objects.all()
    permission_classes = []