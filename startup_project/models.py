from django.db import models

# Create your models here.


class MoscowTransportOrganization(models.Model):
    title = models.CharField(max_length=3000)

    def __str__(self):
        return self.title


class DirectionOfDevelopments(models.Model):
    title = models.CharField(max_length=3000)

    def __str__(self):
        return self.title


class StartupProject(models.Model):
    NEED_CERT_AND_HAVE = 0
    NEED_CERT_AND_DONT_HAVE = 1
    DONT_NEED_CERT = 2
    NEED_CERT_CHOICES = {
        (NEED_CERT_AND_HAVE, 'need_cert_and_have'),
        (NEED_CERT_AND_DONT_HAVE, 'need_cert_and_dont_have'),
        (DONT_NEED_CERT, 'dont_need_cert'),
    }
    title = models.CharField(max_length=1000)
    cert_need = models.IntegerField(choices=NEED_CERT_CHOICES, default=NEED_CERT_AND_HAVE)
    ready_status = models.FloatField(default=0.0)
    small_description = models.CharField(max_length=2000)
    product_usage_cases = models.TextField()
    product_profit = models.TextField()
    msk_transport_organization = models.ForeignKey('startup_project.MoscowTransportOrganization', related_name='msk_trn_org', on_delete=models.SET_NULL, null=True)
    accelerator_request = models.TextField()
    pilot_project_description = models.TextField()
    product_site = models.CharField(max_length=500)
    where_did_u_came = models.TextField()
    presentation_link = models.ForeignKey('media.Media', related_name='presentation', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    direction = models.ForeignKey('startup_project.DirectionOfDevelopments', related_name='startup_project_direction', on_delete=models.SET_NULL, null=True)
    related_user = models.ForeignKey('user.User', related_name='related_user', on_delete=models.SET_NULL, null=True)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.title


class StartupProjectStep(models.Model):
    STATUS_SCREENING = 'screening'
    STATUS_SCROLLING = 'scrolling'
    STATUS_EXPERT_COUNCIL = 'expert_council'
    STATUS_ACCELERATION = 'acceleration'
    STATUS_PILOT = 'pilot'
    STATUS_CHOICES = {
        (0, STATUS_SCREENING),
        (1, STATUS_SCROLLING),
        (2, STATUS_EXPERT_COUNCIL),
        (3, STATUS_ACCELERATION),
        (4, STATUS_PILOT),
    }
    step = models.IntegerField(choices=STATUS_CHOICES, default=0)
    project = models.ForeignKey('startup_project.StartupProject', related_name='startup_project', on_delete=models.CASCADE)


class StartupProjectStepMedia(models.Model):
    step = models.ForeignKey('startup_project.StartupProjectStep', related_name='startup_project_step', on_delete=models.CASCADE)
    media = models.ForeignKey('media.Media', related_name='step_media', on_delete=models.SET_NULL, null=True)


class StartupProjectReviewExpert(models.Model):
    personal_data = models.ForeignKey('user.Person', related_name='expert_person', on_delete=models.SET_NULL, null=True)
    position = models.CharField(max_length=1000)


class StartupProjectReview(models.Model):
    MARKS = {
        ('not_interested', 0),
        ('interested', 1),
        ('want_to_pilot', 2),
    }
    mark = models.IntegerField(choices=MARKS, default=0)
    project = models.ForeignKey('startup_project.StartupProject', related_name='review_startup_project', on_delete=models.CASCADE)
    expert = models.ForeignKey('startup_project.StartupProjectReviewExpert', related_name='expert', on_delete=models.CASCADE)


class StartupMedia(models.Model):
    media = models.ForeignKey('media.Media', related_name='project_media', on_delete=models.SET_NULL, null=True)
    startup_project = models.ForeignKey('startup_project.StartupProject', related_name='media_startup_project', on_delete=models.CASCADE)


class StartupPilotProject(models.Model):
    STATUS_WORKING = 'working'
    STATUS_SUSPENDED = 'suspended'
    STATUS_CLOSED = 'closed'
    STATUS_CANCELED = 'canceled'
    PILOT_STATUS_CHOICES = {
        (STATUS_WORKING, 1),
        (STATUS_SUSPENDED, 2),
        (STATUS_CLOSED, 0),
        (STATUS_CANCELED, 3)
    }
    project = models.ForeignKey('startup_project.StartupProject', related_name='pilot_startup_project', on_delete=models.CASCADE)
    status = models.IntegerField(choices=PILOT_STATUS_CHOICES, default=1)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class Tag(models.Model):
    title = models.CharField(max_length=500)

    def __str__(self):
        return self.title


class StartupTag(models.Model):
    project = models.ForeignKey('startup_project.StartupProject', related_name='tag_startup_project', on_delete=models.CASCADE)
    tag = models.ForeignKey('startup_project.Tag', related_name='tag', on_delete=models.CASCADE)


class StartupPilotProjectTestingPhase(models.Model):
    PHASE_OTKM = 'otkm'
    PHASE_PARAMETERS_DETALIZATION = 'parameters_detalization'
    PHASE_PREPARING_FOR_TESTING = 'preparing_for_testing'
    PHASE_TESTING = 'testing'
    PHASE_TEST_REPORT = 'test_report'
    PHASE_CLOSING_PILOT = 'closing_pilot'
    PHASE_CLOSING = 'closing'
    PHASE_CHOICES = {
        (PHASE_OTKM, 0),
        (PHASE_PARAMETERS_DETALIZATION, 1),
        (PHASE_PREPARING_FOR_TESTING, 2),
        (PHASE_TESTING, 3),
        (PHASE_TEST_REPORT, 4),
        (PHASE_CLOSING_PILOT, 5),
        (PHASE_CLOSING, 6),
    }

    pilot = models.ForeignKey('startup_project.StartupPilotProject', related_name='pilot_project', on_delete=models.CASCADE)
    status = models.IntegerField(choices=PHASE_CHOICES)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)