from django.apps import AppConfig


class StartupProjectConfig(AppConfig):
    name = 'startup_project'
