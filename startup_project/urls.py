from django.urls import path
from . import views


urlpatterns = [
    path('', views.ListCreateStartupsAPIView.as_view(), name='get_startups'),
    path('<int:pk>/', views.RetrieveUpdateDestroyStartupsAPIView.as_view(), name='get_startup'),
]