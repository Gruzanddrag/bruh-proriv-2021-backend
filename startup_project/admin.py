from django.contrib import admin

# Register your models here.
from startup_project.models import MoscowTransportOrganization, DirectionOfDevelopments, StartupProject, Tag, \
    StartupTag, StartupMedia

admin.site.register(MoscowTransportOrganization)
admin.site.register(DirectionOfDevelopments)
admin.site.register(StartupProject)
admin.site.register(Tag)
admin.site.register(StartupTag)
admin.site.register(StartupMedia)

