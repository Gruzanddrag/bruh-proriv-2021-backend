from rest_framework import serializers

from media.models import Media
from startup_project.models import StartupProject, Tag, StartupTag, MoscowTransportOrganization, StartupMedia
from user.serializers import UserSerializer


class StartupProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = StartupProject
        fields = '__all__'


class StartupProjectTagSerializer(serializers.ModelSerializer):
    title = serializers.ReadOnlyField(source='tag.title')

    class Meta:
        model = StartupTag
        fields = ['title']


class MediaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Media
        fields = "__all__"


class StartupProjectMediaSerializer(serializers.ModelSerializer):
    media = MediaSerializer(many=False, read_only=True)

    class Meta:
        model = StartupMedia
        fields = ['media']


class MoscowTransportOrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = MoscowTransportOrganization
        fields = ['title']


class StartupProjectFullSerializer(serializers.ModelSerializer):
    msk_transport_organization = MoscowTransportOrganizationSerializer(many=False, read_only=True)
    media_startup_project = StartupProjectMediaSerializer(many=True, read_only=True)
    tag_startup_project = StartupProjectTagSerializer(many=True, read_only=True)
    presentation_link = MediaSerializer(many=False, read_only=True)
    related_user = UserSerializer(many=False, read_only=True)

    class Meta:
        model = StartupProject
        fields = (
            'title',
            'cert_need',
            'ready_status',
            'small_description',
            'product_usage_cases',
            'product_profit',
            'msk_transport_organization',
            'accelerator_request',
            'pilot_project_description',
            'product_site',
            'where_did_u_came',
            'presentation_link',
            'created_at',
            'updated_at',
            'direction',
            'related_user',
            'tag_startup_project',
            'media_startup_project'
        )
