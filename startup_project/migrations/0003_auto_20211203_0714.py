# Generated by Django 3.1.8 on 2021-12-03 07:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('startup_project', '0002_auto_20211203_0701'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=500)),
            ],
        ),
        migrations.AlterField(
            model_name='startuppilotproject',
            name='status',
            field=models.IntegerField(choices=[('suspended', 2), ('canceled', 3), ('closed', 0), ('working', 1)], default=1),
        ),
        migrations.AlterField(
            model_name='startuppilotprojecttestingphase',
            name='status',
            field=models.IntegerField(choices=[('otkm', 0), ('testing', 3), ('closing_pilot', 5), ('closing', 6), ('preparing_for_testing', 2), ('test_report', 4), ('parameters_detalization', 1)]),
        ),
        migrations.AlterField(
            model_name='startupprojectreview',
            name='mark',
            field=models.IntegerField(choices=[('interested', 1), ('want_to_pilot', 2), ('not_interested', 0)], default=0),
        ),
        migrations.AlterField(
            model_name='startupprojectstep',
            name='step',
            field=models.IntegerField(choices=[('expert_council', 2), ('screening', 0), ('scrolling', 1), ('acceleration', 3), ('pilot', 4)], default=0),
        ),
        migrations.CreateModel(
            name='StartupTag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('project', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tag_startup_project', to='startup_project.startupproject')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tag', to='startup_project.tag')),
            ],
        ),
    ]
