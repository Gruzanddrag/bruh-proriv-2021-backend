from django.db import models

# Create your models here.
from startup_project.models import StartupProjectStep


class SavedSearchResults(models.Model):
    direction = models.ForeignKey('startup_project.DirectionOfDevelopments', related_name='saved_results_project_direction',
                                  on_delete=models.SET_NULL, null=True)
    msk_transport_organization = models.ForeignKey('startup_project.MoscowTransportOrganization',
                                                   related_name='saved_result_msk_trn_org', on_delete=models.SET_NULL, null=True)
    step = models.IntegerField(choices=StartupProjectStep.STATUS_CHOICES, default=0)


class SavedSearchResultsTag(models.Model):
    saved_result = models.ForeignKey('saved_search_results.SavedSearchResults', related_name='saved_result_for_tag', on_delete=models.SET_NULL, null=True)
    tag = models.ForeignKey('startup_project.Tag', related_name='saved_result_tag', on_delete=models.SET_NULL, null=True)
