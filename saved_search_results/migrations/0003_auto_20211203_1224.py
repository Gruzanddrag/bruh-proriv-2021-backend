# Generated by Django 3.1.8 on 2021-12-03 12:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('saved_search_results', '0002_auto_20211203_1129'),
    ]

    operations = [
        migrations.AlterField(
            model_name='savedsearchresults',
            name='step',
            field=models.IntegerField(choices=[(0, 'screening'), (2, 'expert_council'), (4, 'pilot'), (1, 'scrolling'), (3, 'acceleration')], default=0),
        ),
    ]
