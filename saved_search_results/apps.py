from django.apps import AppConfig


class SavedSearchResultsConfig(AppConfig):
    name = 'saved_search_results'
