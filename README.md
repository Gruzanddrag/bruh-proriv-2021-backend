# Ссылки
## Web Frontend
https://gitlab.com/teqst/leadersofdigital_innotim_markup

## IOS
https://github.com/fuzr0dah/leadersofdigitalBruh2021iOS

## Android
https://github.com/TheKicher/BRUH_INNO_TIM_2021


# Проект
Реализованна БД, а также методы получения и сохранения данных о проекте стартапа

Схема:
![alt text](bd_image.png)


# Комманда
- Булахов Никита Романович - *Backend* - https://t.me/bul_nikita
- Авдиенко Алла Дмитриевна - *Аналитик* - https://t.me/Avdienko_Alla
- Татевосян Артем Витальевич - *Web-Frontend* - https://t.me/teqst
- Чеботарев Кирилл Дмитриевич - *Android* - https://t.me/K1cher
- Поляничев Артём Дмитриевич - *IOS* - https://t.me/fuzr0dah

