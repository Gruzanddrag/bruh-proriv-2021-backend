from django.db import models

# Create your models here.


class Person(models.Model):
    first_name = models.CharField(max_length=200)
    second_name = models.CharField(max_length=200)
    third_name = models.CharField(max_length=200)
    phone = models.CharField(max_length=15)
    email = models.EmailField(max_length=300)

    def __str__(self):
        return self.first_name + ' ' + self.second_name + ' ' + self.third_name + ' (' + self.email + ')'


# Модель пользователя системы - имеется в виду участника комманды который подал заявку.
class User(models.Model):
    personal_data = models.ForeignKey('user.Person', related_name='user_person', on_delete=models.SET_NULL, null=True)
    # Будет возможность удалять или приглашать в комманду. ->
    is_organization_administrator = models.BooleanField(default=False)
    organization = models.ForeignKey('organization.Organization', related_name='organization', on_delete=models.SET_NULL, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    position = models.CharField(max_length=1000)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.personal_data.__str__() + ', org: ' + self.organization.__str__()


# Навык пользователя (C#, Управление персоналом и так далее)
class Skill(models.Model):
    title = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class UserSkill(models.Model):
    user = models.ForeignKey('user.User', related_name='skill_user', on_delete=models.CASCADE)
    skill = models.ForeignKey('user.Skill', related_name='skill', on_delete=models.CASCADE)