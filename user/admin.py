from django.contrib import admin

# Register your models here.
from user.models import User, Person

admin.site.register(Person)
admin.site.register(User)
