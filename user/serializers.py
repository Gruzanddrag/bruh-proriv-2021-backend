from rest_framework import serializers

from organization.models import Organization
from user.models import User, Person


class PersonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = "__all__"


class OrganizationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    personal_data = PersonSerializer(many=False, read_only=True)
    organization = OrganizationSerializer(many=False, read_only=True)

    class Meta:
        model = User
        fields = [
            'personal_data',
            'is_organization_administrator',
            'organization',
            'created_at',
            'updated_at',
            'position',
        ]

